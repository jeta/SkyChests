package me.zath.skychests.transmute.interfaces;
/*
 * MC 
 * Created by zAth
 */

public interface Transmutable {

    String getPluginName();

    boolean isMySql();
    SqlProperties getSqlProperties();
    SqlTable getSqlTable();

    SerializeMethods getSerializeMethods();

}
