package me.zath.skychests.transmute.interfaces;
/*
 * MC 
 * Created by zAth
 */

public class SqlProperties {

    private String sqlUser;
    private String sqlHost;
    private int sqlPort;
    private String sqlDatabase;
    private String sqlPassword;
    private String localFileName;

    public SqlProperties(String sqlUser, String sqlHost, int sqlPort, String sqlDatabase, String sqlPassword, String localFileName) {
        this.sqlUser = sqlUser;
        this.sqlHost = sqlHost;
        this.sqlPort = sqlPort;
        this.sqlDatabase = sqlDatabase;
        this.sqlPassword = sqlPassword;
        this.localFileName = localFileName;
    }

    public String getSqlUser() {
        return sqlUser;
    }

    public String getSqlHost() {
        return sqlHost;
    }

    public int getSqlPort() {
        return sqlPort;
    }

    public String getSqlDatabase() {
        return sqlDatabase;
    }

    public String getSqlPassword() {
        return sqlPassword;
    }

    public String getLocalFileName() {
        return localFileName;
    }

    @Override
    public String toString() {
        return "SqlProperties{" +
            "sqlUser='" + sqlUser + '\'' +
            ", sqlHost='" + sqlHost + '\'' +
            ", sqlPort=" + sqlPort +
            ", sqlDatabase='" + sqlDatabase + '\'' +
            ", sqlPassword='" + sqlPassword + '\'' +
            ", localFileName='" + localFileName + '\'' +
            '}';
    }
}
