package me.zath.skychests.transmute.sqllite;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.transmute.interfaces.SerializeMethods;
import me.zath.skychests.transmute.interfaces.SqlProperties;
import me.zath.skychests.transmute.interfaces.SqlTable;
import me.zath.skychests.transmute.interfaces.Transmutable;

public class SQLite implements Transmutable {

    @Override
    public String getPluginName() {
        return "SkyChests";
    }

    @Override
    public boolean isMySql() {
        return false;
    }

    @Override
    public SqlProperties getSqlProperties() {
        return new SqlProperties("", "", 0, "", "", "chests.db");
    }

    @Override
    public SqlTable getSqlTable() {
        return new SqlTable("chests", "owner", "id", "serializedicon", "serializedcontents");
    }

    @Override
    public SerializeMethods getSerializeMethods() {
        return new Methods();
    }
}
