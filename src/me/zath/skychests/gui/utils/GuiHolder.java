package me.zath.skychests.gui.utils;
/*
 * MC
 * Created by zAth
 */
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
public class GuiHolder implements InventoryHolder{

    public enum Type{
        MAIN, ICON, DELETE, BUY, CHEST
    }

    private String dono;
    private Integer id;
    private Type type;

    public GuiHolder(String dono, Integer id, Type type) {
        this.dono = dono;
        this.id = id;
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public String getDono() {
        return dono;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public Inventory getInventory() {
        return null;
    }

}