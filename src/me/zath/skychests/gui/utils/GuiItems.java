package me.zath.skychests.gui.utils;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.utils.Utils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class GuiItems {

    private static ItemStack fill, notOwned, owned, previous, back, next, confirm;

    public static void loadConfirm(String name, List<String> lore, int id, int data){
        confirm = Utils.makeItem(name, lore, id, data);
    }

    public static void loadPrevious(String name, List<String> lore, int id, int data){
        previous = Utils.makeItem(name, lore, id, data);
    }

    public static void loadBack(String name, List<String> lore, int id, int data){
        back = Utils.makeItem(name, lore, id, data);
    }

    public static void loadNext(String name, List<String> lore, int id, int data){
        next = Utils.makeItem(name, lore, id, data);
    }

    public static ItemStack getConfirm() {
        return confirm;
    }

    public static ItemStack getPrevious() {
        return previous;
    }

    public static ItemStack getBack() {
        return back;
    }

    public static ItemStack getNext() {
        return next;
    }

    public static void loadFill(int id, int data){
        fill = Utils.makeItem(id, data);
    }

    public static void loadNotOwned(String name, List<String> lore, int id, int data){
        notOwned = Utils.makeItem(name, lore, id, data);
    }

    public static void loadOwned(String name, List<String> lore, int id, int data){
        owned = Utils.makeItem(name, lore, id, data);
    }

    public static ItemStack getFill() {
        return fill;
    }

    public static ItemStack getNotOwned(int id, String player, Player toOpen) {
        ItemStack toReturn = notOwned.clone();
        ItemMeta toReturnMeta = toReturn.getItemMeta();

        String newName = notOwned.getItemMeta().getDisplayName().replaceAll("%id%", "" + id);
        toReturnMeta.setDisplayName(newName);

        List<String> lore = new ArrayList<>();
        notOwned.getItemMeta().getLore().forEach(string -> {
            if(string.contains("%cost%")) {
                double finalPrice = Utils.getFinalPrice(toOpen);

                if(toOpen.hasPermission("skychests.free"))
                    finalPrice = 0.0;

                Player _player = Utils.getPlayer(player);
                if(_player != null) {
                    finalPrice = Utils.getFinalPrice(_player);
                    if(_player.hasPermission("skychests.free"))
                        finalPrice = 0.0;
                }

                string = string.replaceAll("%cost%", "" + finalPrice);
            }

            lore.add(string);
        });
        toReturnMeta.setLore(lore);

        toReturn.setItemMeta(toReturnMeta);
        return toReturn;
    }

    public static ItemStack getOwned(int id) {
        ItemStack toReturn = owned.clone();
        ItemMeta toReturnMeta = toReturn.getItemMeta();

        toReturnMeta.setLore(owned.getItemMeta().getLore());

        String newName = owned.getItemMeta().getDisplayName().replaceAll("%id%", "" + id);
        toReturnMeta.setDisplayName(newName);
        toReturn.setItemMeta(toReturnMeta);
        return toReturn;
    }

    public static ItemStack getOwned(int typeId, int dataId, int id) {
        ItemStack toReturn = new ItemStack(typeId, 1, (short) dataId);
        ItemMeta toReturnMeta = toReturn.getItemMeta();

        toReturnMeta.setLore(owned.getItemMeta().getLore());

        String newName = owned.getItemMeta().getDisplayName().replaceAll("%id%", "" + id);
        toReturnMeta.setDisplayName(newName);
        toReturn.setItemMeta(toReturnMeta);
        return toReturn;
    }

    public static ArrayList<ItemStack> getIcons() {
        ArrayList<Material> unavailableMaterials = new ArrayList<>();

        unavailableMaterials.add(Material.AIR);
        unavailableMaterials.add(Material.WATER);
        unavailableMaterials.add(Material.STATIONARY_WATER);
        unavailableMaterials.add(Material.LAVA);
        unavailableMaterials.add(Material.STATIONARY_LAVA);
        unavailableMaterials.add(Material.BED_BLOCK);
        unavailableMaterials.add(Material.PISTON_EXTENSION);
        unavailableMaterials.add(Material.PISTON_MOVING_PIECE);
        unavailableMaterials.add(Material.DOUBLE_STEP);
        unavailableMaterials.add(Material.FIRE);
        unavailableMaterials.add(Material.REDSTONE_WIRE);
        unavailableMaterials.add(Material.CROPS);
        unavailableMaterials.add(Material.SIGN_POST);
        unavailableMaterials.add(Material.WOODEN_DOOR);
        unavailableMaterials.add(Material.WALL_SIGN);
        unavailableMaterials.add(Material.IRON_DOOR_BLOCK);
        unavailableMaterials.add(Material.GLOWING_REDSTONE_ORE);
        unavailableMaterials.add(Material.REDSTONE_TORCH_OFF);
        unavailableMaterials.add(Material.SUGAR_CANE_BLOCK);
        unavailableMaterials.add(Material.PORTAL);
        unavailableMaterials.add(Material.CAKE_BLOCK);
        unavailableMaterials.add(Material.DIODE_BLOCK_OFF);
        unavailableMaterials.add(Material.DIODE_BLOCK_ON);
        unavailableMaterials.add(Material.PUMPKIN_STEM);
        unavailableMaterials.add(Material.MELON_STEM);
        unavailableMaterials.add(Material.NETHER_WARTS);
        unavailableMaterials.add(Material.BREWING_STAND);
        unavailableMaterials.add(Material.CAULDRON);
        unavailableMaterials.add(Material.ENDER_PORTAL);
        unavailableMaterials.add(Material.REDSTONE_LAMP_ON);
        unavailableMaterials.add(Material.WOOD_DOUBLE_STEP);
        unavailableMaterials.add(Material.COCOA);
        unavailableMaterials.add(Material.TRIPWIRE);
        unavailableMaterials.add(Material.FLOWER_POT);
        unavailableMaterials.add(Material.CARROT);
        unavailableMaterials.add(Material.POTATO);
        unavailableMaterials.add(Material.SKULL);
        unavailableMaterials.add(Material.REDSTONE_COMPARATOR_ON);
        unavailableMaterials.add(Material.REDSTONE_COMPARATOR_OFF);
        unavailableMaterials.add(Material.STANDING_BANNER);
        unavailableMaterials.add(Material.WALL_BANNER);
        unavailableMaterials.add(Material.DAYLIGHT_DETECTOR_INVERTED);
        unavailableMaterials.add(Material.DOUBLE_STONE_SLAB2);
        unavailableMaterials.add(Material.SPRUCE_DOOR);
        unavailableMaterials.add(Material.BIRCH_DOOR);
        unavailableMaterials.add(Material.JUNGLE_DOOR);
        unavailableMaterials.add(Material.ACACIA_DOOR);
        unavailableMaterials.add(Material.DARK_OAK_DOOR);

        ArrayList<ItemStack> toReturn = new ArrayList<>();

        for (Material material : Material.values()) {
            if (unavailableMaterials.contains(material)) continue;

            ItemStack itemStack;
            switch (material) {
                case STONE:
                    for(int data = 0; data <= 6; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case DIRT:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case WOOD:
                    for(int data = 0; data <= 5; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case SAPLING:
                    for(int data = 0; data <= 5; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case SAND:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case LOG:
                    for(int data = 0; data <= 3; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case LEAVES:
                    for(int data = 0; data <= 3; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case SPONGE:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case SANDSTONE:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case LONG_GRASS:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case QUARTZ_BLOCK:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case STAINED_CLAY:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case STAINED_GLASS:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case STAINED_GLASS_PANE:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case PRISMARINE:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case CARPET:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case DOUBLE_PLANT:
                    for(int data = 0; data <= 5; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case RED_SANDSTONE:
                    for(int data = 0; data <= 2; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case SMOOTH_BRICK:
                    for(int data = 0; data <= 3; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case WOOD_STEP:
                    for(int data = 0; data <= 5; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case COBBLE_WALL:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case COAL:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case GOLDEN_APPLE:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case COOKED_FISH:
                    for(int data = 0; data <= 1; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case RAW_FISH:
                    for(int data = 0; data <= 3; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case INK_SACK:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case MONSTER_EGG:
                    for(int data = 50; data <= 120; data++){
                        if(data == 53 || data == 63 || data == 64 || (data >= 69 && data <= 89) || data == 97 || data == 99 || (data >= 102 && data <= 119)){
                            continue;
                        }
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case STEP:
                    for(int data = 0; data <= 7; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case WOOL:
                    for(int data = 0; data <= 15; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                case RED_ROSE:
                    for(int data = 0; data <= 8; data++){
                        itemStack = new ItemStack(material, 1, (short) data );
                        toReturn.add(itemStack);
                    }

                    break;
                default:
                    itemStack = new ItemStack(material, 1, (short) 0);
                    toReturn.add(itemStack);
                    break;
            }
        }

        return toReturn;

    }

}
