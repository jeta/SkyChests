package me.zath.skychests.objects;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.gui.utils.GuiHolder;
import me.zath.skychests.utils.Serialize;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Chest {

    private String dono;
    private Integer id;
    private ItemStack icon;
    private Inventory inventory;
    private String serializedContents;
    private String serializedIcon;

    private boolean dirty;

    public Chest(String dono, Integer id, String serializedIcon, String serializedContents) {
        this.dono = dono;
        this.id = id;
        this.icon = Serialize.simpleDeserialize(serializedIcon);
        this.serializedIcon = serializedIcon;
        this.serializedContents = serializedContents;

        String title = Config.getGui_ChestTitle().replaceAll("%id%", "" + id).replaceAll("%dono%", dono);
        this.inventory = SkyChests.getSkychests().getServer().createInventory(new GuiHolder(dono, id, GuiHolder.Type.CHEST), 6*9, title);
        for(ItemStack itemStack : Serialize.fromBase64List(serializedContents)){
            if(itemStack == null || itemStack.getType() == Material.AIR) continue;
            this.inventory.addItem(itemStack);
        }

        this.dirty = false;

        SkyChests.getChestArrayList().add(this);
    }

    public Chest(String dono, Integer id, String serializedIcon, ItemStack icon, String serializedContents, ItemStack[] contents) {
        this.dono = dono;
        this.id = id;
        this.icon = icon;
        this.serializedIcon = serializedIcon;
        this.serializedContents = serializedContents;

        String title = Config.getGui_ChestTitle().replaceAll("%id%", "" + id).replaceAll("%dono%", dono);
        this.inventory = SkyChests.getSkychests().getServer().createInventory(new GuiHolder(dono, id, GuiHolder.Type.CHEST), 6*9, title);
        for(ItemStack itemStack : contents){
            if(itemStack == null || itemStack.getType() == Material.AIR) continue;
            this.inventory.addItem(itemStack);
        }

        this.dirty = false;

        SkyChests.getChestArrayList().add(this);
    }

    public boolean isDirty(){
        return dirty;
    }

    public void setDirty(boolean dirty){
        this.dirty = dirty;
    }

    public String getDono() {
        return dono;
    }

    public Integer getId() {
        return id;
    }

    public ItemStack getIcon() {
        return icon;
    }

    public void setIcon(ItemStack icon) {
        this.icon = icon;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public String getSerializedContents() {
        return serializedContents;
    }

    public void setSerializedContents(String serializedContents) {
        this.serializedContents = serializedContents;
    }

    public String getSerializedIcon() {
        return serializedIcon;
    }

    public void setSerializedIcon(String serializedIcon) {
        this.serializedIcon = serializedIcon;
    }

    @Override
    public String toString() {
        return "Chest{" +
            "dono='" + dono + '\'' +
            ", id=" + id +
            ", serializedContents='" + serializedContents + '\'' +
            ", serializedIcon='" + serializedIcon + '\'' +
            '}';
    }
}
