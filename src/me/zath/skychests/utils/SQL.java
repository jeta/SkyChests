package me.zath.skychests.utils;
/*
 * MC
 * Created by zAth
 */


import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.objects.Chest;

import java.io.File;
import java.sql.*;

public class SQL {

    private static Connection connection;
    public static File db;

    private static Connection getConnection() {
        return connection;
    }

    public static void closeConnection() {
        try {
            if (connection == null || connection.isClosed()) return;

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void openConnection(boolean force) {
        if (Config.getSql_Enabled()) {
            try {
                if (!force && connection != null && !connection.isClosed()) return;

                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(
                    "jdbc:mysql://" + Config.getSql_Host() + ":" + Config.getSql_Port() + "/" + Config.getSql_Database(),
                    Config.getSql_User(),
                    Config.getSql_Password());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                if (!force && connection != null && !connection.isClosed()) return;

                Class.forName("org.sqlite.JDBC");
                connection = DriverManager.getConnection("jdbc:sqlite:" + db.getAbsolutePath());
            } catch (ClassNotFoundException | SQLException ex8) {
                ex8.printStackTrace();
            }
        }
    }

    public static void createTable(int tries) {
        if(tries >= 5){
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel criar a tabela");
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel criar a tabela");
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel criar a tabela");
            return;
        }

        openConnection(false);

        try {
            PreparedStatement sql = connection.prepareStatement("CREATE TABLE IF NOT EXISTS chests(owner varchar(255), id tinyint, serializedicon varchar(255), serializedcontents text);");

            sql.execute();
            sql.close();
        } catch (SQLException ex) {
            if (Config.getSql_ShowPossibleErros()) ex.printStackTrace();
            openConnection(true);

            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + "§4Erro criando a tabela");
            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + "§4Criando a tabela pela" + (tries + 1) + " vez...");
            createTable(++tries);
        }
    }

    public static void create(Chest chest, int tries) {
        if(tries >= 5){
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel criar o bau");
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel criar o bau");
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel criar o bau");
            return;
        }

        openConnection(false);

        try {
            PreparedStatement statement = getConnection().prepareStatement("INSERT INTO chests values(?,?,?,?);");
            statement.setString(1, chest.getDono());
            statement.setInt(2, chest.getId());
            statement.setString(3, chest.getSerializedIcon());
            statement.setString(4, chest.getSerializedContents());

            statement.execute();
            statement.close();
        } catch (Exception e) {
            if (Config.getSql_ShowPossibleErros()) e.printStackTrace();
            openConnection(true);

            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + "§4Erro criando o bau");
            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + chest.toString());
            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + "§4Criando o bau pela" + (tries + 1) + " vez...");
            create(chest, ++tries);
        }
    }

    public static void delete(String owner, Integer id, int tries) {
        if(tries >= 5){
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel deletar o bau");
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel deletar o bau");
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel deletar o bau");
            return;
        }

        openConnection(false);

        try {
            PreparedStatement statement = getConnection().prepareStatement("DELETE FROM chests WHERE owner=? AND id=?;");
            statement.setString(1, owner);
            statement.setInt(2, id);

            statement.execute();
            statement.close();
        } catch (Exception e) {
            if (Config.getSql_ShowPossibleErros()) e.printStackTrace();
            openConnection(true);

            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + "§4Erro apagando o bau");
            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + owner + "," + id);
            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + "§4Apagando o bau pela" + (tries + 1) + " vez...");
            delete(owner, id, ++tries);
        }
    }

    public static void update(String owner, Integer id, String serializedIcon, String serializedContents, int tries) {
        if(tries >= 5){
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel atualizar o bau");
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel atualizar o bau");
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel atualizar o bau");
            return;
        }

        openConnection(false);

        try {
            PreparedStatement statement = getConnection().prepareStatement("UPDATE chests SET serializedicon=?, serializedcontents=? WHERE owner=? AND (id=?)");
            statement.setString(1, serializedIcon);
            statement.setString(2, serializedContents);
            statement.setString(3, owner);
            statement.setInt(4, id);

            statement.execute();
            statement.close();
        } catch (Exception e) {
            if (Config.getSql_ShowPossibleErros()) e.printStackTrace();
            openConnection(true);

            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + "§4Erro atualizando o bau");
            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + owner + "," + id + "," + serializedIcon + "," + serializedContents);
            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + "§4Atualizando o bau pela" + (tries + 1) + " vez...");
            update(owner, id, serializedIcon, serializedContents, ++tries);
        }
    }

    public static void initialize(int tries) {
        if(tries >= 5){
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel carregar os baus");
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel carregar os baus");
            SkyChests.getSkychests().getServer().getLogger().severe("§4Não foi possivel carregar os baus");
            return;
        }

        openConnection(false);

        try {
            PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM chests;");

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {

                String owner = rs.getString("owner");
                Integer id = rs.getInt("id");
                String serializedIcon = rs.getString("serializedicon");
                String serializedContents = rs.getString("serializedcontents");

                new Chest(owner, id, serializedIcon, serializedContents);
            }

            statement.close();
            rs.close();
        } catch (Exception e) {
            if (Config.getSql_ShowPossibleErros()) e.printStackTrace();
            openConnection(true);

            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + "§4Erro carregando os baus");
            SkyChests.getSkychests().getServer().getConsoleSender().sendMessage(SkyChests.getSkychests().getDescription().getName() + "§4Carregandos os baus pela" + (tries + 1) + " vez...");
            initialize(++tries);
        }
    }

}
