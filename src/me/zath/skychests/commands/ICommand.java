package me.zath.skychests.commands;
/*
 * MC
 * Created by zAth
 */

import me.zath.skychests.SkyChests;
import org.bukkit.command.CommandSender;

public interface ICommand {

    void execute(CommandSender sender, String[] args, SkyChests plugin);

    boolean supportsConsole();

    String getPermission();

    String getDescription();
}
