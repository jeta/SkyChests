package me.zath.skychests.commands;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.controllers.ChestController;
import me.zath.skychests.gui.MainGui;
import me.zath.skychests.objects.Chest;
import me.zath.skychests.utils.Utils;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Cmd implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("bau")){
            Player p = (Player) sender;
            if(p.hasPermission("skychests.open") && args.length == 0){
                p.openInventory(MainGui.getPlayerGui(p.getName().toLowerCase(), p));
                p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
            } else if(p.hasPermission("skychests.open.each") && args.length > 0){
                if(!Utils.isInt(args[0])){
                    p.sendMessage(Config.getMsg_ChestNotSpecified());
                    return true;
                }
                if(!ChestController.hasChestId(sender.getName().toLowerCase(), Integer.parseInt(args[0]))){
                    p.sendMessage(Config.getMsg_ChestNotOwned());
                    return true;
                }
                Chest chest = ChestController.get(sender.getName().toLowerCase(), Integer.parseInt(args[0]));
                p.openInventory(chest.getInventory());
                p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
            } else {
                p.sendMessage(Config.getCmd_NoPermission());
                return true;
            }
        }
        return false;
    }
}
