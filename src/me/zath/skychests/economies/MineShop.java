package me.zath.skychests.economies;
/*
 * MC 
 * Created by zAth
 */

import com.mineshop.api.MineSHOP;
import me.zath.skychests.SkyChests;
import org.bukkit.entity.Player;

public class MineShop implements IEconomy {

    private MineSHOP mineSHOP;

    public MineShop(SkyChests skyChests) {
        mineSHOP = new MineSHOP(skyChests);
    }

    @Override
    public void withdraw(Player player, double amount) {
        mineSHOP.setPontos(player.getName(), (int) Math.round(getBalance(player) - amount));
    }

    @Override
    public double getBalance(Player player) {
        return mineSHOP.getPontos(player.getName());
    }

}
