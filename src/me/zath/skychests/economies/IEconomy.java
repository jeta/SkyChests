package me.zath.skychests.economies;
/*
 * MC 
 * Created by zAth
 */

import org.bukkit.entity.Player;

public interface IEconomy {

    void withdraw(Player player, double amount);
    double getBalance(Player player);

}
