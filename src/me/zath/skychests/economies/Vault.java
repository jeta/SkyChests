package me.zath.skychests.economies;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.SkyChests;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class Vault implements IEconomy {

    private Economy economy;

    public Vault(SkyChests skyChests) {
        RegisteredServiceProvider<Economy> economyProvider = skyChests.getServer().getServicesManager().getRegistration(Economy.class);
        if (economyProvider != null){
            economy = economyProvider.getProvider();
        }
    }

    @Override
    public void withdraw(Player player, double amount) {
        economy.withdrawPlayer(player, amount);
    }

    @Override
    public double getBalance(Player player) {
        return economy.getBalance(player);
    }

}
