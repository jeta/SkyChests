package me.zath.skychests.economies;
/*
 * MC 
 * Created by zAth
 */

import org.bukkit.entity.Player;

public class SkyBank implements IEconomy {

    @Override
    public void withdraw(Player player, double amount) {
        me.zath.skybank.SkyBank.getEconomy().withdraw(player.getName(), amount);
    }

    @Override
    public double getBalance(Player player) {
        if(!me.zath.skybank.SkyBank.getEconomy().hasBalance(player.getName()))
            return 0.0;

        return me.zath.skybank.SkyBank.getEconomy().getBalance(player.getName());
    }

}
