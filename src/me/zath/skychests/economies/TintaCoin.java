package me.zath.skychests.economies;
/*
 * MC 
 * Created by zAth
 */

import me.tintacoin.api.API;
import org.bukkit.entity.Player;

public class TintaCoin implements IEconomy {

    @Override
    public void withdraw(Player player, double amount) {
        API.removeSaldo(player.getName().toLowerCase(), amount);
    }

    @Override
    public double getBalance(Player player) {
        return API.getSaldo(player.getName().toLowerCase());
    }

}
