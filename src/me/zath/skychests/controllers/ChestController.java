package me.zath.skychests.controllers;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.Config;
import me.zath.skychests.SkyChests;
import me.zath.skychests.gui.utils.GuiItems;
import me.zath.skychests.objects.Chest;
import me.zath.skychests.utils.SQL;
import me.zath.skychests.utils.Serialize;
import me.zath.skychests.utils.Utils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class ChestController {

    public static void loadChests(){
        SQL.initialize(0);
    }

    public static void update(Chest chest){
        if(Utils.isEmpty(chest.getInventory())) {
            chest.setSerializedContents(Serialize.toBase64List(new ItemStack[] {new ItemStack(Material.AIR)}));
        } else {
            chest.setSerializedContents(Serialize.toBase64List(chest.getInventory().getContents()));
        }
        chest.setSerializedIcon(Serialize.simpleSerialize(chest.getIcon()));

        SQL.update(chest.getDono(), chest.getId(), chest.getSerializedIcon(), chest.getSerializedContents(), 0);
    }

    public static void start(){
        new BukkitRunnable() {
            @Override
            public void run() {
                if(SkyChests.getChestArrayList().isEmpty()) return;
                SkyChests.getChestArrayList()
                    .stream()
                    .filter(Chest::isDirty)
                    .forEach(chest -> {
                            chest.setDirty(false);
                            update(chest);
                        }
                    );
            }
        }.runTaskTimerAsynchronously(SkyChests.getSkychests(), 0, Config.getChest_SaveDelay() * 20);
    }

    public static Chest create(String dono, Integer id){
        Chest chest = new Chest(dono, id, Serialize.simpleSerialize(GuiItems.getOwned(id)), Serialize.toBase64List(new ItemStack[] {new ItemStack(Material.AIR)}));
        SQL.create(chest, 0);

        return chest;
    }

    public static Chest get(String dono, int id){
        Chest toReturn = null;
        for(Chest chest : SkyChests.getChestArrayList()){
            if((chest.getDono().equalsIgnoreCase(dono)) && (chest.getId() == id)) toReturn = chest;
        }

        return toReturn;
    }

    public static Boolean hasChests(String dono){
        for(int i = 0; i < 28; i++){
            if(hasChestId(dono, i)) return true;
        }

        return false;
    }

    public static Boolean hasChestId(String dono, int id){
        return get(dono, id) != null;
    }

    public static int getChestsAmount(String dono){
        int toReturn = 0;
        if(!hasChests(dono)) return toReturn;

        for(int id = 0; id < 28; id++){
            if(hasChestId(dono, id)) toReturn++;
        }

        return toReturn;
    }

}
