package me.zath.skychests.controllers;
/*
 * MC 
 * Created by zAth
 */

import me.zath.skychests.SkyChests;
import me.zath.skychests.economies.*;

public class EconomyController {

    private IEconomy iEconomy;

    public EconomyController(SkyChests skyChests, String economy) {
        if (economy.equalsIgnoreCase("VAULT"))
            iEconomy = new Vault(skyChests);
        else if (economy.equalsIgnoreCase("PLAYERPOINTS"))
            iEconomy = new PlayerPoints(skyChests);
        else if (economy.equalsIgnoreCase("PICOMOEDAS"))
            iEconomy = new PicoMoedas();
        else if (economy.equalsIgnoreCase("MINESHOP"))
            iEconomy = new MineShop(skyChests);
        else if (economy.equalsIgnoreCase("SKYBANK"))
            iEconomy = new SkyBank();
        else if (economy.equalsIgnoreCase("TINTACOIN"))
            iEconomy = new TintaCoin();
        else if (economy.equalsIgnoreCase("MINEPAG"))
            iEconomy = new MinePag();
        else
            iEconomy = null;

        skyChests.getServer().getConsoleSender().sendMessage(skyChests.getName() + " §2Using " + economy +" economy");
    }

    public IEconomy getEconomy() {
        return iEconomy;
    }
}
